import java.io.File
import java.sql.DriverManager
import java.sql.SQLException

fun main(args: Array<String>) {
    val url = "jdbc:postgresql://${args[0]}:${args[1]}/${args[2]}"
    val user = args[3]
    val password = args[4]

    try {
        DriverManager.getConnection(url, user, password).use { connection ->
            val statement = connection.createStatement()
            val query1 = "SELECT s.first_name, s.last_name, AVG(e.grade) as average_grade " +
                    "FROM students s " +
                    "JOIN exams e ON s.id = e.student_id " +
                    "GROUP BY s.id, s.first_name, s.last_name " +
                    "ORDER BY average_grade DESC " +
                    "LIMIT 1"
            val resultSet1 = statement.executeQuery(query1)
            while (resultSet1.next()) {
                val result = "Student: ${resultSet1.getString("first_name")} ${resultSet1.getString("last_name")}, " +
                        "Average Grade: ${resultSet1.getDouble("average_grade")}"
                println(result)
                File("result.txt").printWriter().use { out -> out.println(result) }
            }
        }
    } catch (e: SQLException) {
        e.printStackTrace()
    }
}
