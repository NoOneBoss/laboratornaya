FROM gradle:7.6.1-jdk17

WORKDIR /app
COPY . .

CMD ["gradle", "run", "--args=172.19.0.2 5432 root root root"]
